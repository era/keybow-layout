function handle_key_00(pressed)
    if pressed then
        keybow.tap_key(keybow.LEFT_ARROW, pressed)
    end
end

function handle_key_01(pressed)
    if pressed then
        keybow.tap_key(keybow.DOWN_ARROW, pressed)
    end
end

function handle_key_02(pressed)
    if pressed then
        keybow.tap_key(keybow.RIGHT_ARROW, pressed)
    end
end

function handle_key_03(pressed)
    if pressed then
        keybow.tap_key("`", pressed)
    end
end

function handle_key_04(pressed)
    if pressed then
        keybow.tap_key(keybow.UP_ARROW, pressed)
    end
end

function handle_key_05(pressed)
    if pressed then
        keybow.tap_key("3", pressed)
    end
end

function handle_key_06(pressed)
    if pressed then
        keybow.tap_key("4", pressed)
    end
end

function handle_key_07(pressed) -- originally 5
    if pressed then
        keybow.text("---\ntitle: \n---")
        keybow.tap_key(keybow.UP_ARROW, pressed)
        keybow.tap_key(keybow.LEFT_ARROW, pressed)
    end
end

function handle_key_08(pressed) -- navigates to next TMUX window
    if pressed then
        keybow.set_modifier(keybow.LEFT_CTRL, keybow.KEY_DOWN)
        keybow.tap_key("b", pressed)
        keybow.set_modifier(keybow.LEFT_CTRL, keybow.UP)
        keybow.tap_key("n", pressed)
    end
end

function handle_key_09(pressed) -- originally 7
    if pressed then
        keybow.text("```\n\n\n```")
        keybow.tap_key(keybow.UP_ARROW, pressed)
    end
end

function handle_key_10(pressed)
    if pressed then
        keybow.text("``")
        keybow.tap_key(keybow.LEFT_ARROW, pressed)
    end
end

function handle_key_11(pressed) -- creates a new TMUX window
    if pressed then
        keybow.set_modifier(keybow.LEFT_CTRL, keybow.KEY_DOWN)
        keybow.tap_key("b", pressed)
        keybow.set_modifier(keybow.LEFT_CTRL, keybow.UP)
        keybow.tap_key("c", pressed)
    end
end